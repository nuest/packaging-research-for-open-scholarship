# Packaging research for Open Scholarship

Slides and notes for talks on Open Science, Reproducible Research, Open Scholarship, and research compendia.

## January 2019, Jena

`slides/nuest_packaging-research-for-open-scholarship_jena-jan-2019.Rmd`

[PDF](slides/nuest_packaging-research-for-open-scholarship_jena-jan-2019.pdf)

Invited talk at the [GIScienc group](https://www.geographie.uni-jena.de/en/Chairs/GIScience.html) at the Institute for Geography, Friedrich Schiller University Jena.

**Abstract**

> Scientific breakthroughs are build on previous research.
> Open Science allows everyone to read (Open Access), contribute to (Open Peer Review), extend upon (Open Data, Open Source), improve (Open Methodology), and teach (Open Educational Resources) the latest research results.
> Transparency and collaboration are on their way to become the norm across all academic disciplines.
> Changes are sometimes driven from within, but also imposed from the outside (funders, journals).
> This fast pace poses challenges to researchers of all career stages who must adapt their habits and workflows, and continuously update their toolboxes.
> In this talk, we take a brief tour de force across open scholarship's building blocks, people's roles, and best practices.
> Then we explore different packaging mechanisms that help scientists to improve the reproducibility of their research: research compendia, containerisation or virtual environments can help graduate students, post-docs or professors to increase the quality, efficiency, and reach of their research, collaborations, reviews, and teaching.

## Material

Extended slide set: `nuest_packaging-research-for-open-scholarship.Rmd`